<?php

class Site_sentry 
{

	// private $host   = '192.168.10.10:3139';
	// private $dbname = 'digivladev';
	// private $user   = 'Basing!^21';
	// private $pass   = 'db_msk_application_roys';
	private $host   = '172.16.0.4';
	private $dbname = 'digivla';
	private $user   = 'pdf';
	private $pass   = 'KkkO@)2osl2';
	private $db     = null;

	public function connect_to_db() {
		if ($this->db == null) {
			try {
				$this->db = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->user, $this->pass);
				echo "connected";
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}

		return $this->db;
	}

	public function disconnect_from_db() {
		return $this->db = null;
	}

	public function activity_save($activity, $article_id, $cat_id = '')
	{		  
		$client		= $this->get_client_id();
		$user_id	= $this->get_uid();
	
		$data_insert = array(
			'tanggal'     => date('Y-m-d'),
			'activity'    => $activity,
			'client_id'   => $client, 
			'user_id'     => $user_id,
			'article_id'  => $article_id,
			'category_id' => $cat_id,
		);

		$pdo = $this->connect_to_db();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "INSERT INTO tbl_activity(tanggal, activity, client_id, user_id, article_id, category_id) VALUES ?, ?, ?, ?, ?, ?";
		$data = $pdo->prepare($sql);
		$data->execute($data_insert);
		$this->disconnect_from_db();
		return $data;
	}

	public function get_key()
	{		
		$key  = $_SESSION['usr_keys'];
		return $key; 
	}	
	
	public function get_uid()
	{		
		$uid  = $_SESSION['usr_uid'];
		return $uid; 
	}
	
	public function get_client_id()
	{		
		$uid  = $_SESSION['usr_client_id'];
		return $uid; 
	}
	
	public function get_data_api($code, $data)
	{
		$result 	= false;
		$key    	= $this->get_key();
		$uid    	= $this->get_uid();
		$client_id  = $this->get_client_id();
		if(!empty($code) and !empty($key) and !empty($uid) and is_array($data))
		{
			$url = "http://apps.antara-insight.id/article/detail";
			$serialize      = serialize($data);
			$encode_data     = base64_encode($serialize);
			
			$_params = [
				"a"         => '' . $code . '',
				"keys"      => '' . $key . '',
				"uid"       => '' . $uid . '',
				"client_id" => '' . $client_id . '',
				"data"      => $encode_data,
			];

			$params = [];
			$send_params = http_build_query($params);
			
			$result_raw = $this->get_content_post($url, $params);
				
			$result		 = json_decode($result_raw,1);
		}
		return $result;
	}
	
	public function get_content_post($url,$data ) 
	{ 
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 49);
		ob_start();
		curl_exec ($ch);
		curl_close ($ch);
		$string = ob_get_contents();
		ob_end_clean();
		return $string;
	}
}

// $obj = new Site_sentry();
// $obj->connect_to_db();
