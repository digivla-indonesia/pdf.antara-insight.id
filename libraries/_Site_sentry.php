<?php

Class Site_sentry 
{

	function Site_sentry()
	{
		$this->obj =& get_instance();
	}
	
	function get_key()
	{		
		$key  = $this->obj->session->userdata('usr_keys');
		return $key; 
	}	
	
	function get_uid()
	{		
		$uid  = $this->obj->session->userdata('usr_uid');
		return $uid; 
	}
	
	function get_client_id()
	{		
		$uid  = $this->obj->session->userdata('usr_client_id');
		return $uid; 
	}
	
	function get_data_api($code, $data)
	{
	  	//echo "<pre>";
  		//echo print_r($data);
 		//echo "</pre>";
		$result 	= false;
		$key    	= $this->get_key();
		$uid    	= $this->get_uid();
		$client_id  = $this->get_client_id();
		if(!empty($code) and !empty($key) and !empty($uid) and is_array($data))
		{
			$url = "http://192.168.10.119/mskapi/api.php?";
			//$url = "http://localhost/mskapi/api.php?";
			//$url = "";
			$serialize      = serialize($data);
		   
			$encode_data     = base64_encode($serialize);
			
			$params = array(
				"a" => ''.$code.'',
				"keys" => ''.$key.'',
				"uid" => ''.$uid.'',
				"client_id"=>''.$client_id.'',
				"data" => $encode_data,
			);
			$send_params = http_build_query($params);
			//if($code!='004U')
			//{
				//$result_raw	 = file_get_contents($url."".$send_params);
			//}
			//else
			//{
				$result_raw = $this->get_content_post($url,$params );
				
			//}
			$result		 = json_decode($result_raw,1);
			//print_r($result);
		}
  		//echo "<pre>";
	  	//echo print_r($result);
 		//echo "</pre>";
		return $result;
	}
	
	function get_content_post($url,$data ) 
	{ 
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 49);
		ob_start();
		curl_exec ($ch);
		curl_close ($ch);
		$string = ob_get_contents();
		ob_end_clean();
		return $string;
	}
}