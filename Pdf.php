<?php

namespace Create;

require_once 'helpers/fpdf17/fpdf_helper.php';
require_once 'helpers/fpdi/fpdi_helper.php';
require_once 'helpers/phpword_helper.php';
require_once 'libraries/Site_sentry.php';
require_once 'api/Api.php';

use FPDI;
use PHPWord;
use PHPWord_Style_Cell;
use PHPWord_IOFactory;
use Digivla\Api as API;

class Pdf {

  public function get_original_all() { 
    # $stat             = htmlspecialchars($_POST['stat']);
    # $arr_article_id = explode(",", htmlspecialchars($_POST['article']));
    # $arr_article_id   = implode(',', $_POST['article']);
    # $formate          = htmlspecialchars($_POST['formate']);
    # $logo_head        = htmlspecialchars($_POST['logo_head']);
    
    # $stat             = $_SERVER['stat'];
    # $arr_article_id   = $_SERVER['article'];
    # $formate          = $_SERVER['formate'];
    # $logo_head        = $_SERVER['logo_head'];
    
    parse_str($_SERVER["QUERY_STRING"]);

    if($formate == 1) {
      $pdf = new FPDI();
    } else {
      $PHPWord = new PHPWord();
    }

    $rand =  time();
    //foreach($arr_article_id as $article_id)
    //{
      // if($article_id!='' and $stat!='')
      if($stat!='')
      {
        # $code = '014';
        # $data = array("article_id" => $article_id);
        # $datanee = $this->site_sentry->get_data_api($code, $data);
        
        $api = new API;
        # $datanee = $api->get_api_article('['.$arr_article_id.']');
        $datanee = $api->get_api_article('['.$article.']', $client_id);
        $datanee = (array)$datanee;
        
        $data_client = $api->get_data_client('['.$article.']', $client_id);
        $data_client = (array)$data_client;
        $data_client = $data_client[0]->_source;
        # print_r($data_client); exit();
        
        $newsvalue = $data_client->advalue_bw;
        $advalue = ceil($data_client->advalue_bw * 0.34);
        
        # print_r((array)$datanee);exit(); 
        # print_r($datanee); exit();
        # if( $datanee['code'] == 00 and $datanee['data'] != NULL)
        if(!empty($datanee))
        {
          # foreach($datanee['data'] as $pie) {
          foreach($datanee as $pie) {
          $pie = (array)$pie->_source;
          $dateene = strtotime($pie['datee']);
          $datene = date('d-M-Y',$dateene);
          $tone = '';
          if($pie['tone'] == -1) { $tone = 'Negatif'; }
          if($pie['tone'] == 0) { $tone = 'Netral'; }
          if($pie['tone'] == 1) { $tone = 'Positif'; }
          
          $pagee = $pie['page'];
          $pagee = preg_replace('/^0+(.)/', "$1", $pagee);
          
          $fonte = 8;
          $cont_title = strlen($pie['title']);
          if($cont_title > 65 and $cont_title < 100) { $fonte = 6; }
          if($cont_title > 100) { $fonte = 5; }
          if(strpos($pie['file_pdf'], '.pdf') !== false)
          {
            $sepa = explode('-', $pie['file_pdf']);
            $year = $sepa[0];
            $month  = $sepa[1];
            $date = $sepa[2];
            $folder = $stat == 1 ? 'pdf_text' : 'pdf_images';
          }
          
          if($formate==1)
          {
            //logging activity
            # $this->site_sentry->activity_save("pdf_scan",$article_id,''); 
                
                $pdf->AddPage('P', 'A4');
                //$template =  "./asset/article_template.pdf"; 
                //$pagecount = $pdf->setSourceFile($template);
                
                // if(file_exists('./admin/asset/images/' . $_SESSION['logo_head'] . ''))
                if(file_exists('./admin/asset/images/' . $logo_head . ''))
                {
                  //get size of image
                  // list($width, $height) = getimagesize('./admin/asset/images/' . $_SESSION['logo_head'] . '');
                  list($width, $height) = getimagesize('./admin/asset/images/' . $logo_head . '');
                  $ceke = $width/$height;
                  $a = 9; $b = 14; $c = 10;
                  if($ceke<=4 and $ceke>=2.5){$a = 10.3; $b = 14; $c = 11;}
                  if($ceke<2.5 and $ceke>=2){$a = 14; $b = 14; $c = 14;}
                  if($ceke<2){$a = 21; $b = 14; $c = 15;}
                  // $pdf->Image('./admin/asset/images/' . $_SESSION['logo_head'] . '', $a, $b, 0, $c);
                  $pdf->Image('./admin/asset/images/' . $logo_head . '', $a, $b, 0, $c);
                }
                
                $pdf->SetXY(50, 11);
                $pdf->SetFont('Arial','', 8);
                $pdf->Cell(12,5,'Judul','T L B','0','');
                $pdf->Cell(2,5,':','T B','0','L');
                $pdf->SetFont('Arial','', $fonte);
                $pdf->Cell(140,5,$pie['title'],'T R B','1','');
                
                $pdf->SetXY(50, 16);
                $pdf->SetFont('Arial','', 8);
                $pdf->Cell(12,5,'Media','L B','0','L');
                $pdf->Cell(2,5,':','B','0','L');
                $pdf->Cell(68,5,$pie['media_name'],'R B','0','L');
                $pdf->Cell(25,5,'Wartawan','B','0','L');
                $pdf->Cell(2,5,':','B','0','L');
                $pdf->Cell(45,5,$pie['journalist'],'R B','1','L');
                
                $pdf->SetXY(50, 21);
                $pdf->SetFont('Arial','', 8);
                $pdf->Cell(12,5,'Tanggal','L B','0','L');
                $pdf->Cell(2,5,':','B','0','L');
                $pdf->Cell(68,5,$datene,'R B','0','L');
                $pdf->Cell(25,5,'Nada Pemberitaan','B','0','L');
                $pdf->Cell(2,5,':','T B','0','L');
                $pdf->Cell(45,5,$tone,'R B','1','L');
                
                $pdf->SetXY(50, 26);
                $pdf->SetFont('Arial','', 8);
                $pdf->Cell(12,5,'Halaman','L B','0','L');
                $pdf->Cell(2,5,':','B','0','L');
                $pdf->Cell(68,5,$pagee,'R B','0','L');
                $pdf->Cell(25,5,'NewsValue','B','0','L');
                $pdf->Cell(2,5,':','T B','0','L');
                # $pdf->Cell(45,5,ceil($pie['newsvalue_bw']),'R B','1','L');
                $pdf->Cell(45,5,$newsvalue,'R B','1','L');
                
                $pdf->SetXY(50, 31);
                $pdf->SetFont('Arial','', 8);
                $pdf->Cell(12,5,'','L B','0','L');
                $pdf->Cell(2,5,'','B','0','L');
                $pdf->Cell(68,5,'','R B','0','L');
                $pdf->Cell(25,5,'AdValue','B','0','L');
                $pdf->Cell(2,5,':','T B','0','L');
                # $pdf->Cell(45,5,ceil($pie['advalue_bw']),'R B','1','L');
                $pdf->Cell(45,5,$advalue,'R B','1','L');
              
              if(strpos($pie['file_pdf'],'.pdf') !== false)
              {
                // $filee  = "./pdf/".$folder."/".$year."/".$month."/".$date."/".$pie['file_pdf']; 
                $filee  = "./".$folder."/".$year."/".$month."/".$date."/".$pie['file_pdf']; 
                // $filee  = $folder."/".$year."/".$month."/".$date."/".$pie['file_pdf']; 
                if(file_exists($filee))
                {               
                  //Set the source PDF file
                  $pagecount = $pdf->setSourceFile($filee);
                  //Import the first page of the file
                  $tpl = $pdf->importPage(1);
                  $gtw = $pdf->getTemplateSize($tpl, 0, 0, 0);
                  if ($gtw['h'] > 200){
                    $gtw['h'] = 200;
                    }
                  if ($gtw['w'] > 200){
                    $gtw['w'] = 195;
                    }
                  $side = 10;
                  if($gtw['w'] <= 190)
                    {
                      $side = 100-($gtw['w']/2)+5;
                    }
                  $pdf->useTemplate($tpl, $side, 43, $gtw['w'], $gtw['h']);
                  $pdf->SetXY(10, 11);
                  $pdf->Cell(45,15,'','T L','0','');
                  $pdf->SetXY(10, 16);
                  $pdf->Cell(45,15,'','','0','');
                  $pdf->SetXY(10, 21);
                  $pdf->Cell(45,15,'','B L','0','');
                  
                  if($pagecount>1)
                  {
                    for($nom=2;$nom<=$pagecount;$nom++)
                    {                 
                      $height1 = $gtw['h'];
                      
                      $tpl = $pdf->importPage($nom);
                      $gtw = $pdf->getTemplateSize($tpl, 0, 0, 0);
                      
                      if($height1+$gtw['h'] >= 260)
                      {
                        $pdf->AddPage('P', 'A4');
                        ${$nom} = 10 ;
                      }
                      else
                      {
                        ${$nom} = $height1+45;
                      }
                      
                      if ($gtw['h'] > 200){
                        $gtw['h'] = 200;
                        }
                      if ($gtw['w'] > 200){
                        $gtw['w'] = 195;
                        }
                      $side = 10;
                      if($gtw['w'] <= 190)
                        {
                          $side = 100-($gtw['w']/2)+5;
                        }
                      $pdf->useTemplate($tpl, $side, ${$nom}, $gtw['w'], $gtw['h']);
                    }
                  }
                }
                else
                {
                  $pdf->Write('Empty File');
                }
              }
              else
              {
                $contentna = str_replace("<br />",chr(10)."".chr(13),$pie['content']);
                $pdf->Write(5,$contentna);
              }
            }
            else
            {
              //logging activity
              # $this->site_sentry->activity_save("doc",$article_id,'');

              
              $style = array('marginLeft'=>450, 'marginRight'=>450, 'marginTop'=>600, 'marginBottom'=>100);
              $section = $PHPWord->createSection($style);
              // $logone = './admin/asset/images/' . $_SESSION['logo_head'];
              $logone = './admin/asset/images/' . $logo_head;
              list($wilo, $helo) = getimagesize($logone);
              $wilo = ceil($wilo/1.1);
              $helo = ceil($helo/1.1);
              
              $wilo = $wilo>100?100:$wilo;
              $helo = $helo>100?100:$helo;
              
              //text
              $styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>5);
              $styleCell = array('valign'=>'center');
              $colspan = array('valign'=>'center', 'gridSpan' => 2);
              $rowspan1 = array('valign'=>'center', 'vMerge' => 'restart');
              $rowspan2 = array('valign'=>'center', 'vMerge' => 'fusion');
              $styleCellBTLR = array('valign'=>'center', 'textDirection'=>PHPWord_Style_Cell::TEXT_DIR_BTLR);
              $fontStyle = array('bold'=>false, 'align'=>'center', 'color'=>'006699');
              $PHPWord->addTableStyle('myOwnTableStyle', $styleTable);
              $table = $section->addTable('myOwnTableStyle');
              $PHPWord->addLinkStyle('myOwnLinkStyle', array('bold'=>true, 'color'=>'808000'));
              $table->addRow();
              $table->addCell(2000, $rowspan1)->addImage($logone, array('width'=>$wilo, 'height'=>$helo, 'align'=>'center'));
              $table->addCell(10000, $colspan)->addText('Judul : '.$pie['title'].'', $fontStyle);
              $table->addRow();
              $table->addCell(2000, $rowspan2);
              $table->addCell(4000, $styleCell)->addText('Media : '.$pie['media_name'].' ', $fontStyle);
              $table->addCell(4000, $styleCell)->addText('Wartawan : '.$pie['journalist'].' ', $fontStyle);
              $table->addRow();
              $table->addCell(2000, $rowspan2);
              $table->addCell(4000, $styleCell)->addText('Tanggal : '.$datene.'', $fontStyle);
              $table->addCell(4000, $styleCell)->addText('Nada Pemberitaan : '.$tone.'', $fontStyle);
              $table->addRow();
              $table->addCell(2000, $rowspan2);
              if(strpos($pie['file_pdf'],'.pdf') !== false)
              {
                // $table->addCell(4000, $styleCell)->addText('Halaman : '.$pagee.'', $fontStyle);
                $table->addCell(4000, $styleCell)->addText('Halaman : '.$pie['page'].'', $fontStyle);
              }
              else
              { 
                // $table->addCell(4000, $styleCell)->addLink($pie['link_url'], 'Lihat (Ctrl+Klik)', 'myOwnLinkStyle');  
                $table->addCell(4000, $styleCell)->addLink($pie['file_pdf'], 'Lihat (Ctrl+Klik)', 'myOwnLinkStyle');  
              }
              # $table->addCell(4000, $styleCell)->addText('NewsValue : '.ceil($pie['newsvalue_bw']), $fontStyle);
              $table->addCell(4000, $styleCell)->addText('NewsValue : '.$newsvalue, $fontStyle);
              $table->addRow();
              $table->addCell(2000, $rowspan2);
              $table->addCell(4000, $styleCell)->addText('', $fontStyle);
              $table->addCell(4000, $styleCell)->addText('AdValue: '.$advalue, $fontStyle);
              $section->addTextBreak(1);
              
              //image
              //$how = isset($sepa[9])?$sepa[9]:1;
              //$how = preg_replace('/^0+(.)/', "$1", $how);
              if(strpos($pie['file_pdf'],'.pdf') !== false)
              {
                $maine = substr($pie['file_pdf'], 0,-4);
                $nome  = 1 ;
                for($a=1;$a<=4;$a++)
                {
                  $av = sprintf("%02s", $a);
                  // $filee  =  "./pdf/image_jpg/".$year."/".$month."/".$date."/".$maine."-01-".$av.".jpg";
                  
                  
                  $filee  =  "./jpg_images/".$year."/".$month."/".$date."/".$maine."-01-".$av.".jpg";
                  // $filee  = "./".$folder."/".$year."/".$month."/".$date."/".$pie['file_pdf']; 
                  
                  
                  
                  // $filee  =  "jpg_images/".$year."/".$month."/".$date."/".$maine."-01-".$av.".jpg";
                  if(file_exists($filee))
                  {
                    $nome = $av; 
                    //break;
                  }
                }
                
                for($va=1;$va<=$nome;$va++)
                {
                  $vane  = sprintf("%02s", $va);
                  $howne = sprintf("%02s", $nome);
                  $eks   = $vane.'-'.$howne;
                  // $filee  =  "./pdf/image_jpg/".$year."/".$month."/".$date."/".$maine."-".$eks.".jpg";
                  
                  
                  
                  $filee  =  "./jpg_images/".$year."/".$month."/".$date."/".$maine."-".$eks.".jpg";
                  // $filee  = "./".$folder."/".$year."/".$month."/".$date."/".$pie['file_pdf'];
                  
                  
                  // $filee  =  "jpg_images/".$year."/".$month."/".$date."/".$maine."-".$eks.".jpg";
                  //$filee  = "./cobah3.jpg";             
                  if(file_exists($filee))
                    {
                    list($width, $height) = getimagesize($filee);
                    $width = ceil($width/2);
                    $height = ceil($height/2);            
                    $width = $width>750?749:$width;
                    $height = $height>890?889:$height;      
                    $section->addImage($filee, array('width'=>$width, 'height'=>$height, 'align'=>'center'));
                    }
                else
                  {
                		printf('Empty File');
                  }   
                }
              }
              else
              {
                $contentna = str_replace("<br />",chr(10)."".chr(13),$pie['content']);
                $section->addText($contentna);
              }
              
              
              
            }
          }
        }
        else
        {
          echo "invalid...!!!!";
        }
      }
      else
      {
        echo "I am sorry";
      } 
    //}
    
    if($formate == 1) { 
      $pdf->Output('article_ori_'.$rand.'.pdf', 'D');
    }
    else
    {
      $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');           
      header("Content-type: application/vnd.ms-word");
      header('Content-Disposition: attachment; filename=article_word_'.$rand.'.docx');
      $objWriter->save('php://output');             
      exit;  
    }
    
  }

}
