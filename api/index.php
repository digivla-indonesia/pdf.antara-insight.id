<?php

require_once 'Api.php';

use \Digivla\Api as Api;

$api = new Api;
$arr_article_id = [253632227,253632277,253632292,253634299,253634353];

echo json_encode($api->get_api_article($arr_article_id));