<?php

namespace Digivla;

class Api {

  private $username = 'digivla';
  private $password = 'nzVV2$/(zTH~>m3V';

  public function get_api_token() {
    // $ch = curl_init('http://182.23.64.70:8000/token');
    $ch = curl_init('http://apps.antara-insight.id/token');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
        )
    );                                                         
    $json = curl_exec($ch);
    $result = json_decode($json);
    return $result;
  }

  public function get_media_list(){
	$data_api_token = $this->get_api_token();
	$token = $data_api_token->data;

	$data_string = '{"token":"';
	$data_string .= $token.'","statuse": ["N", "A"]}';

	$ch = curl_init('http://apps.antara-insight.id/tb_media');
	curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
	);                                                         
	$json = curl_exec($ch);
	$result = json_decode($json);

	return $result->data;
  }

  public function get_data_client($article_id, $client_id){
	$data_api_token = $this->get_api_token();
	$token = $data_api_token->data;

	$data_string = '{"token":"';
	$data_string .= $token.'","article_id":';
	$data_string .= $article_id;
	$data_string .= ',"client_id":"'.$client_id.'",';
	$data_string .= '"date_from": "2010-01-01", "date_to": "'.date('Y-m-d').'", "limit": 1000}';

	$ch = curl_init('http://apps.antara-insight.id/data_client/search');
	curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
	);                                                         
	$json = curl_exec($ch);
	$result = json_decode($json);

	return $result->data->hits->hits;
  }

  public function get_api_article($arr_article_id, $client_id) {
    $data_api_token = $this->get_api_token();
    $token = $data_api_token->data;
    // echo $token; exit();

    # $data_string = json_encode($data);                                         
    
    $data_string = '{"token":"';
    $data_string .= $token.'","article_id":';
    $data_string .= $arr_article_id.'}';
    # print_r($data_string);exit();
    
    // $ch = curl_init('http://182.23.64.70:8000/article/detail');
    $ch = curl_init('http://apps.antara-insight.id/article/detail');
    curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );                                                         
    $json = curl_exec($ch);
    $result = json_decode($json);
    
    $data_client = $this->get_data_client($arr_article_id, $client_id);
    $media_list = $this->get_media_list();
    
    foreach ($result->data->hits->hits as $obj) {
    	$obj = $obj->_source;

    	foreach($data_client as $dc){
    		$dc = $dc->_source;

    		if($obj->article_id === $dc->article_id){
    			$obj->tone = $dc->tone;
    			$obj->newsvalue_fc = $dc->advalue_fc;
    			$obj->newsvalue_bw = $dc->advalue_bw;
    			$obj->advalue_fc = $dc->advalue_fc * 0.34;
    			$obj->advalue_bw = $dc->advalue_bw * 0.34;
    		}
    	}
    	
    	foreach($media_list as $ml){
			if($obj->media_id == $ml->media_id){
				$obj->media_name = $ml->media_name;
			}
		}
	}

    return $result->data->hits->hits;
  }

  public function insert_tbl_activity($activity = 'inter', $user_id = NULL, $client_id = NULL, $article_id = NULL, $category_id = NULL) {

    $data_api_token = $this->get_api_token();
    $token = $data_api_token->data;

    $data = array(
        'token'       => $token,
        'tanggal'     => date('Y-m-d'),
        'activity'    => $activity,
        'user_id'     => $user_id,
        'client_id'   => $client_id,
        'article_id'  => $article_id,
        'category_id' => $category_id
    );

    $data_string = json_encode($data);                                         
    $ch = curl_init('http://apps.antara-insight.id/insert/tbl_activity');
    curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );                                                         
    $json = curl_exec($ch);
    $result = json_decode($json);
    return $result;
  }
}
